import requests
import json
from types import SimpleNamespace
from notify_run import Notify
import datetime as dt
import time
import itertools

class Colors:
  HEADER = '\033[95m'
  OKBLUE = '\033[94m'
  OKCYAN = '\033[96m'
  OKGREEN = '\033[92m'
  WARNING = '\033[93m'
  FAIL = '\033[91m'
  ENDC = '\033[0m'
  BOLD = '\033[1m'
  UNDERLINE = '\033[4m'

notify = Notify(endpoint='https://notify.run/0uvLIsoqvHoFFR5R')
notify.info()

print('Only Covaxin Dose 2 Channel')
covaxinNotify = Notify(endpoint='https://notify.run/TnlXCDU4L5jsloZQ')
covaxinNotify.info()

def loop():
  districtIds = [
    '548',    # Tirunelveli
    '305',    # Kozhikode
    '297',    # Kannur
    '477'     # Mahe
  ]
  dates = [
    dt.date.today().strftime('%d-%m-%Y'),                         # Today
    (dt.date.today() + dt.timedelta(1)).strftime('%d-%m-%Y'),     # Tomorrow
    (dt.date.today() + dt.timedelta(2)).strftime('%d-%m-%Y')      # Day after tomorrow
  ]
  apiUrls = []
  messageBuffer = []
  message = ''
  prevMessage = ''

  covaxinD2MessageBuffer = []
  covaxinD2Message = ''
  prevCovaxinD2Message = ''

  for date in dates:
    for district in districtIds:
      apiUrls.append('https://cdn-api.co-vin.in/api/v2/appointment/sessions/public/calendarByDistrict?district_id='+ district +'&date=' + date)

  for apiUrl in apiUrls:
    # print(f'[{Colors.OKBLUE}INFO{Colors.ENDC}] Fetching Data for District-{apiUrl[-19:-16]} on {apiUrl[-10:]}')
    page = requests.get(apiUrl)
    jsonData = json.loads(page.content, object_hook=lambda d: SimpleNamespace(**d))
    availableCenters = []

    try:
      for center in jsonData.centers:
        temp = {
          'name': center.name,
          'centerName': center.address,
          'district': center.district_name,
          'sessions': []
        }
        for session in center.sessions:
          if(session.available_capacity > 0):
            temp['sessions'].append({
              'sessionDate': session.date,
              'sessionMinAge': session.min_age_limit,
              'availableDoses': session.available_capacity,
              'vaccine': session.vaccine,
              'dose1': session.available_capacity_dose1,
              'dose2': session.available_capacity_dose2
            })
        if len(temp['sessions']) > 0:
          availableCenters.append(temp)

      for center in availableCenters:
        covishieldCount = 0
        covaxinCount = 0
        covaxinDose2Count = 0
        temp = ''
        temp += '|#| ' + center['name'] + ' |-| ' + center['centerName'] + '\n'

        for session in center['sessions']:
          if (session['vaccine'].strip() == 'COVISHIELD'):
            covishieldCount += session['dose1'] + session['dose2']
          elif (session['vaccine'].strip() == 'COVAXIN'):
            if (session['dose2'] > 0):
              covaxinDose2Count += session['dose2']
            covaxinCount += session['dose1'] + session['dose2']
        temp += '|#| COVISHIELD - ' + str(covishieldCount) + ' |-| COVAXIN - ' + str(covaxinCount) + '\n\n'
        messageBuffer.append(temp)
        if (covaxinCount > 0):
          covaxinD2MessageBuffer.append(temp)
      # print(f'[{Colors.OKGREEN}SUCCESS{Colors.ENDC}] Done Processing District-{apiUrl[-19:-16]} for {apiUrl[-10:]}')
    except:
      print(f'[{Colors.FAIL}ERROR{Colors.ENDC}] Could not Fetch Data for District-{apiUrl[-19:-16]} on {apiUrl[-10:]}')

    time.sleep(3)

  for msg in messageBuffer:
    message += msg
  
  for msg in covaxinD2MessageBuffer:
    covaxinD2Message += msg

  if (message != prevMessage):
    print(f'[{Colors.OKGREEN}SUCCESS{Colors.ENDC}] New Data')
    print('|#| VACCINES |#|\n\n' + message)
    if (covaxinDose2Count > 0 and covaxinD2Message != prevCovaxinD2Message):
      covaxinNotify.send('|#| VACCINES |#|\n\n ' + covaxinD2Message)
    notify.send('|#| VACCINES |#|\n\n ' + message)
    print(f'[{Colors.OKCYAN}INFO{Colors.ENDC}] Sent Notification')
    prevMessage = message
  else:
    print(f'[{Colors.WARNING}WARN{Colors.ENDC}] Processed Same Data as last time')

for i in itertools.count(start=1):
  loop()
  print(f'[{Colors.OKCYAN}INFO{Colors.ENDC}] Completed Loop{i}! Waiting 300 secs for next iteraton...')
  time.sleep(300)
